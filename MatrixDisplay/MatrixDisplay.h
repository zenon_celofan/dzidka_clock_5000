#ifndef MATRIXDISPLAY_H_
#define MATRIXDISPLAY_H_


#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"
#include <cmath>


extern u8 matrix_digit[][8];
extern u8 matrix_char[][8];



template <u8 number_of_led_modules = 1>
class MatrixDisplay {
private:
	class Pin	matrix_din_pin;
	class Pin	matrix_cs_pin;
	class Pin	matrix_clk_pin;

	void	push_16_bits(u16 packet);
	void	send_line_data(u8 line_number, u8* pixels);
	void	send_command_to_all_modules(u8 addr, u8 data);


public:
	u8 screen[number_of_led_modules][8] = {};

	MatrixDisplay(u16 matrix_din_pin = PB7, u16 matrix_cs_pin = PB8, u16 matrix_clk_pin = PB9);

	void	shutdown(bool set_shutdown = true);
	void	set_brightness(u8 value = 0);
	void	show_screen(void);
	void	test_mode(bool set_test_mode = true);
	void	scan_limit(u8 max = 0x07);
	void	clear_display(void);
	void 	reset(void);
	void	display_number(u16 number);
	void	display_string(const char * string);
	void	clear_screen(void);
	void	blink_dots(void);
	void	invert_screen(void);



};

#include "bluepill.h"
#include <stdlib.h>



template <u8 number_of_led_modules>
MatrixDisplay<number_of_led_modules>::MatrixDisplay(u16 matrix_din_pin, u16 matrix_cs_pin , u16 matrix_clk_pin) :
  matrix_din_pin(matrix_din_pin, GPIO_Mode_Out_OD),
  matrix_cs_pin(matrix_cs_pin, GPIO_Mode_Out_OD),
  matrix_clk_pin(matrix_clk_pin, GPIO_Mode_Out_OD) {

	reset();
} //MatrixDisplay()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::set_brightness(u8 value) {
	if (value > 0x0F)
		value = 0x0F;

	send_command_to_all_modules(0x0A, value);
} //set_brightness()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::push_16_bits(u16 packet) {
	for (s8 i = 15; i >= 0; i--) {
		if ((packet >> i & 1) != 0) {
			matrix_din_pin.digital_write(Bit_SET);
		} else {
			matrix_din_pin.digital_write(Bit_RESET);
		}
		matrix_clk_pin.digital_write(Bit_RESET);
		matrix_clk_pin.digital_write(Bit_SET);
	}
} //push_8_bits()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::send_command_to_all_modules(u8 addr, u8 data) {

	u16 frame = (addr << 8) + data;

	matrix_cs_pin.digital_write(Bit_SET);
	matrix_din_pin.digital_write(Bit_RESET);
	matrix_clk_pin.digital_write(Bit_RESET);
	matrix_cs_pin.digital_write(Bit_RESET);

	for (u8 module = 0; module < number_of_led_modules; module++) {
		push_16_bits(frame);
	}

	matrix_cs_pin.digital_write(Bit_SET);
} //send_command


template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::send_line_data(u8 line_number, u8* pixels) {
	//line_number &= 0b00001111;

	matrix_cs_pin.digital_write(Bit_SET);
	matrix_din_pin.digital_write(Bit_RESET);
	matrix_clk_pin.digital_write(Bit_RESET);
	matrix_cs_pin.digital_write(Bit_RESET);

	for (u8 module = 0; module < number_of_led_modules; module++) {
		u16 frame = ( (line_number+1) << 8) + pixels[module];
		push_16_bits(frame);
	}

	matrix_cs_pin.digital_write(Bit_SET);

} //send_data()



template <u8 number_of_led_modules>
void	MatrixDisplay<number_of_led_modules>::show_screen(void) {
	u8 line_data[number_of_led_modules];

	for (u8 line_number = 0; line_number < 8; line_number++) {
		for (u8 module = 0; module < number_of_led_modules; module++) {
			line_data[module] = screen[module][line_number];
		}
		send_line_data(line_number, line_data);
	}
} //display()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::shutdown(bool set_shutdown) {
	if (set_shutdown == true)
		send_command_to_all_modules(0x0C, 0x00);
	else
		send_command_to_all_modules(0x0C, 0x01);
} //shutdown()



template <u8 number_of_led_modules>
void  MatrixDisplay<number_of_led_modules>::test_mode(bool set_test_mode) {
	if (set_test_mode == true)
		send_command_to_all_modules(0x0F, 0x01);
	else
		send_command_to_all_modules(0x0F, 0x00);
} //display_test()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::clear_display(void) {
	for (u8 line = 1; line <= 8; line++) {
		send_command_to_all_modules(line, 0x00);
	}
} //clear_display()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::scan_limit(u8 max) {
	send_command_to_all_modules(0x0B, max);
} //scan_limit()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::reset(void) {
	shutdown(true);
	shutdown(false);
	test_mode(false);
	scan_limit();
	set_brightness(0);
	clear_display();
} //reset()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::display_number(u16 number) {
	u8 digit;

	clear_screen();

	if (number >= pow(10, number_of_led_modules)) {
		return;
	}

	for (s8 module = number_of_led_modules - 1; module >= 0; module--) {
		digit = number % 10;
		number /= 10;

		for (u8 line_number = 0; line_number < 8; line_number++) {
			screen[module][line_number] = matrix_digit[digit][line_number];
		}

		if (number == 0) {
			return;
		}
	}
} //display_number()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::display_string(const char * string) {

	clear_screen();

	for (s8 module = number_of_led_modules - 1; module >= 0; module--) {

		for (u8 line_number = 0; line_number < 8; line_number++) {
			screen[module][line_number] = matrix_char[string[module] - 'A'][line_number];
		}
	}
} //display_string()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::invert_screen(void) {

	for (s8 module = number_of_led_modules - 1; module >= 0; module--) {

		for (u8 line_number = 0; line_number < 8; line_number++) {
			screen[module][line_number] = ~(screen[module][line_number]);
		}
	}
} //invert_screen()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::clear_screen(void) {

	for (u8 module = 0; module < number_of_led_modules; module++) {
		for (u8 line_number = 0; line_number < 8; line_number++) {
			screen[module][line_number] = 0;
		}
	}

} //clear_screen()



template <u8 number_of_led_modules>
void MatrixDisplay<number_of_led_modules>::blink_dots(void) {
	screen[0][0] = screen[0][0] & (1 << 7) ? screen[0][0] - (1 << 7) : screen[0][0] + (1 << 7);
	screen[0][7] = screen[0][7] & (1 << 7) ? screen[0][7] - (1 << 7) : screen[0][7] + (1 << 7);

	screen[number_of_led_modules-1][0] = screen[number_of_led_modules-1][0] & (1 << 0) ? screen[number_of_led_modules-1][0] - (1 << 0) : screen[number_of_led_modules-1][0] + (1 << 0);
	screen[number_of_led_modules-1][7] = screen[number_of_led_modules-1][7] & (1 << 0) ? screen[number_of_led_modules-1][7] - (1 << 0) : screen[number_of_led_modules-1][7] + (1 << 0);

	show_screen();
} //blink_dots()
#endif // MATRIXDISPLAY_H_
