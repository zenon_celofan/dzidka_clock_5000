#include "MembraneKeyboard4x3.h"
#include "Pin.h"
#include "stm32f10x_gpio.h"


#define DEBOUNCE_ITERATIONS		200


char membrane_key[4][3] = {
		{'1', '2', '3'},
		{'4', '5', '6'},
		{'7', '8', '9'},
		{'*', '0', '#'}
};



char key_buffer = 0;
char key_temp;
u8 debounce = 0;
bool is_key_handled = false;



MembraneKeyboard4x3::MembraneKeyboard4x3(u8 row_pin_0, u8 row_pin_1, u8 row_pin_2, u8 row_pin_3, u8 column_pin_0, u8 column_pin_1, u8 column_pin_2) {
  row_pin[0] = Pin(row_pin_0, GPIO_Mode_Out_PP);
  row_pin[1] = Pin(row_pin_1, GPIO_Mode_Out_PP);
  row_pin[2] = Pin(row_pin_2, GPIO_Mode_Out_PP);
  row_pin[3] = Pin(row_pin_3, GPIO_Mode_Out_PP);

  column_pin[0] = Pin(column_pin_0, GPIO_Mode_IPD);
  column_pin[1] = Pin(column_pin_1, GPIO_Mode_IPD);
  column_pin[2] = Pin(column_pin_2, GPIO_Mode_IPD);

} //MembraneKeyboard4x3()



void MembraneKeyboard4x3::poll_keyboard(void) {
	char key = 0;

	for (u8 row_iterator = 0; row_iterator < 4; row_iterator++) {
		row_pin[row_iterator].digital_write(Bit_SET);

		for (u8 column_iterator = 0; column_iterator < 3; column_iterator++) {
			if (column_pin[column_iterator].digital_read() == Bit_SET) {
				key = membrane_key[row_iterator][column_iterator];
			}
		}

		row_pin[row_iterator].digital_write(Bit_RESET);
	}

	if (key != key_buffer) {
		if (key == key_temp) {
			if (debounce < DEBOUNCE_ITERATIONS) {
				debounce++;
			} else {
				key_buffer = key_temp;
				is_key_handled = false;
				debounce = 0;
			}
		} else {
			key_temp = key;
			debounce = 0;
		}
	}

} //poll_keyboard()



char MembraneKeyboard4x3::get_key(void) {
	if (is_key_handled == true) {
		return 0;
	} else {
		is_key_handled = true;
		return key_buffer;
	}
} //get_key()
