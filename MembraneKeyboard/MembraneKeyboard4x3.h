#ifndef MEMBRANEKEYBOARD4X3_H_
#define MEMBRANEKEYBOARD4X3_H_

#include "bluepill.h"
#include "Pin.h"

//extern char membrane_key[4][3];


class MembraneKeyboard4x3 {

private:
	class Pin row_pin[4];
	class Pin column_pin[3];

public:
		 MembraneKeyboard4x3(u8 row_pin_0, u8 row_pin_1, u8 row_pin_2, u8 row_pin_3, u8 column_pin_0, u8 column_pin_1, u8 column_pin_2);

	void poll_keyboard(void);
	char get_key(void);

};

#endif /* MEMBRANEKEYBOARD4X3_H_ */
