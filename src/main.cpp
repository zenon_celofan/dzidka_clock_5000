#include <stdio.h>
#include <stdlib.h>
#include "bluepill.h"
#include "millis.h"
#include "Pin.h"
#include "Led.h"
#include "MatrixDisplay.h"
#include "MembraneKeyboard4x3.h"
#include "Timers.h"
#include "Piezo.h"


void led_display_update(void);
void blink_led(void);
void short_beep(void);
void long_beep(void);
void blink_led_dots(void);
void double_beep(void);
void blink_screen(void);


#define READY_STATE 		1
#define COUNTING_STATE		2
#define ALARM_STATE			3

#define MINUTE_IN_MS		60000

Led led13;
MatrixDisplay<2> led_matrix(PB7, PB8, PB9);
MembraneKeyboard4x3 keyboard(PA5, PA6, PA7, PB0, PB1, PB10, PB11);
Piezo buzzer(PA1);
Timers<4> my_timers;
u8 state;
u8 minutes = 0;
u8 set_time;
u32 temp_time;
Pin siren(PA0);


int main() {

	millis_init();

	siren.digital_write(0);

	led13.turn_off();

	state = READY_STATE;

	char key;

	led_matrix.display_number(0);
	led_matrix.show_screen();

	while (1) {
		key = keyboard.get_key();

		//READY_STATE
		if (state == READY_STATE) {
			if (key == '#') {
				short_beep();

				if (minutes > 0) {
					set_time = minutes;
					state = COUNTING_STATE;
					led13.turn_on();
					my_timers.attach(0, 500, blink_led_dots);
					temp_time = millis();
				}
			} else if ( (key >= '0') && (key <= '9') ) {
				buzzer.beep(1500,10);
				set_time %= 10;
				set_time *= 10;
				set_time += (key - 0x30);
				minutes = set_time;
				led_matrix.display_number(minutes);
				led_matrix.show_screen();
			}

		//COUNTING_STATE
		} else if (state == COUNTING_STATE) {
			if (key == '#') {
				set_time = 0;
				state = READY_STATE;
				double_beep();
				led13.turn_off();
				my_timers.attach(0, 0, blink_led_dots);
			} else {
				if ( (millis() - temp_time) >= MINUTE_IN_MS) {
					temp_time += MINUTE_IN_MS;
					led_matrix.display_number(--minutes);
					led_matrix.show_screen();

					if (minutes == 0) {
						state = ALARM_STATE;
						led_matrix.display_string("XX");
						led_matrix.show_screen();
						led13.turn_off();
						my_timers.attach(0, 0, blink_led_dots);
						siren.digital_write(1);
						my_timers.attach(2, 1000, blink_screen);
					}
				}
			}

		//ALARM_STATE
		} else if (state == ALARM_STATE) {
			if (key != 0) {
				minutes = set_time;
				set_time = 0;
				double_beep();
				siren.digital_write(0);
				my_timers.attach(2, 0, blink_screen);
				led_matrix.display_number(minutes);
				led_matrix.show_screen();
				state = READY_STATE;
			}
		}

		my_timers.process();
		keyboard.poll_keyboard();
	}
} //main()



void led_display_update(void) {
	if (keyboard.get_key() != 0) {
		led_matrix.display_number(keyboard.get_key() - 0x30);
	} else {
		led_matrix.display_number(random(100));
	}
	led_matrix.show_screen();
} //led_display_update()



void blink_led(void) {
	led13.toggle();
} //blink_led()



void short_beep(void) {
	buzzer.beep(1500, 100);
} //short_beep()



void long_beep(void) {
	buzzer.beep(1500, 500);
} //long_beep()



void blink_led_dots(void) {
	led_matrix.blink_dots();
} //blink_led_dots()



void double_beep(void) {
	short_beep();
	my_timers.attach(2, 150, short_beep, 1);
} //double_beep()



void blink_screen(void) {
	led_matrix.invert_screen();
	led_matrix.show_screen();
} //blink_screen()
